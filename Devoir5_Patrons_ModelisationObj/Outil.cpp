    #include "Outil.h"

Outil::Outil(string nom, int anneeAchat, Photo* photo, string dommage, bool empruntable, double valeurEstime)
{

}

Outil::Outil(BuilderOutil builder) 
{
    this->nom = builder.getNom();
    this->anneeAchat = builder.getAnnee();
    this->photo = builder.getPhoto();
    this->dommageRecu = builder.getDommage();
    this->evaluations = builder.getEvaluation();
    this->empruntable = builder.getEmpruntable();
    this->valeurEstime = builder.getValeurEstime();
}

list<Evaluation*> Outil::getEvaluations()
{
    return this->evaluations;
}

void Outil::ajouterEvaluationListe(Evaluation* evaluation)
{
    this->evaluations.push_back(evaluation);
}


//getters
string Outil::getNom()
{
    return nom;
}

int Outil::getAnnee()
{
    return anneeAchat;
}

Photo* Outil::getPhoto()
{
    return photo;
}

string Outil::getDommage()
{
    return dommageRecu;
}

bool Outil::getEmpruntable()
{
    return empruntable;
}

double Outil::getValeurEstime()
{
    return valeurEstime;
}
