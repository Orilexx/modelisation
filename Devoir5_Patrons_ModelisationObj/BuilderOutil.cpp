#include "BuilderOutil.h"

Outil* BuilderOutil::build()
{

	return new Outil(*this);
}

void BuilderOutil::ajouterEvaluationListe(Evaluation* evaluation)
{
	this->Liste_evaluations.push_back(evaluation);
}


//getters
string BuilderOutil::getNom()
{
	return _nom;
}

int BuilderOutil::getAnnee()
{
	return _anneeAchat;
}

Photo* BuilderOutil::getPhoto()
{
	return _photo;
}

string BuilderOutil::getDommage()
{
	return _dommageRecu;
}

list<Evaluation*> BuilderOutil::getEvaluation()
{
	return this->Liste_evaluations;
}

bool BuilderOutil::getEmpruntable()
{
	return _empruntable;
}

double BuilderOutil::getValeurEstime()
{
	return _valeurEstime;
}


//setters
void BuilderOutil::setNom(string nom)
{
	this->_nom = nom; 
}

void BuilderOutil::setAnneeAchat(int annee)
{
	this->_anneeAchat = annee;
}

void BuilderOutil::setPhoto(Photo* photo)
{
	this->_photo = photo; 
}

void BuilderOutil::setDommageRecu(string dommage)
{
	this->_dommageRecu = dommage;
}

/*
void BuilderOutil::setEvaluation(Evaluation* evaluation)
{
	this->_evaluation = evaluation;
}
*/

void BuilderOutil::setEmpruntable(bool empruntable)
{
	this->_empruntable = empruntable;
}

void BuilderOutil::setValeurEstime(double valeur)
{
	this->_valeurEstime = valeur;
}
