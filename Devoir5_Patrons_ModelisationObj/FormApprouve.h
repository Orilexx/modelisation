#pragma once
#ifndef FORMAPPROUVE_H
#define FORMAPPROUVE_H

#include "Emprunt.h"
#include "Strategy.h"

#include <list>
#include <string>

using namespace std;


class FormApprouve : public Strategy {

public: Emprunt FillEmprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt, Constat* constatRetour, bool demandeApprouve);


};
#endif // !FORMAPPROUVE_H