#include "Emprunt.h"
#include "Strategy.h"
#include "FormNonApprouve.h"

#include <list>
#include <string>


Emprunt FormNonApprouve::FillEmprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt = nullptr, Constat* constatRetour = nullptr, bool demandeApprouve = false)
{
	return Emprunt(emprunteur, preteur, outils, dateEmprunt, dateRetour, nullptr, nullptr, false);
}
