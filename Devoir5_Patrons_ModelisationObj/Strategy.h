#pragma once
#ifndef STRATEGY_H
#define STRATEGY_H
#include "Emprunt.h"

using namespace std;
class Strategy
{
public:
	virtual ~Strategy() {}
	virtual Emprunt FillEmprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt, Constat* constatRetour, bool demandeApprouve) = 0;
};
#endif //STRATEGY_H