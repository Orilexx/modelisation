#include "FacadeEvaluation.h"


FacadeEvaluation::FacadeEvaluation(Utilisateur* utilisateur)
{
    list<Emprunt*> emprunts;
    

    this->utilisateur = utilisateur;
    this->outils = utilisateur->getOutils();
    this->emprunts = utilisateur->getEmpruntsPreteur();
    this->emprunts.splice(this->emprunts.end(), utilisateur->getEmpruntsEmprunteur());
}

list<Evaluation*> FacadeEvaluation::ensembleEvaluationsFaitesSurUtilisateur()
{
    list<Evaluation*> evaluations;
    //Constat constat;

    evaluations = utilisateur->getEvaluationProprietaire();
    evaluations.splice(evaluations.end(), utilisateur->getEvaluationsEmprunteur());
    for (Outil* outil : outils)
    {
       evaluations.splice(evaluations.end(), outil->getEvaluations());
    }
    for (Emprunt* emprunt : emprunts) 
    {
        Constat constat = *emprunt->getConstatEmprunt();
        if (constat.getEvaluateur() != this->utilisateur)
        {
            evaluations.push_back(&constat);
        }

        constat = *emprunt->getConstatRetour();
        if (constat.getEvaluateur() != this->utilisateur)
        {
            evaluations.push_back( &constat);
        }
    }
    
    return evaluations;
}
