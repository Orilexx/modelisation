#pragma once
#ifndef CONTEXT_H
#define CONTEXT_H
#include "Strategy.h"
#include "Emprunt.h"

using namespace std;
class Strategy;
class Context
{

private:
	Strategy *strategy_;

public:

    Context(Strategy* strategy = nullptr) : strategy_(strategy)
    {
    }
    ~Context()
    {
        delete this->strategy_;
    }
	void set_strategy(Strategy* strategy);
	Emprunt FillEmprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt, Constat* constatRetour, bool demandeApprouve);

};
#endif //CONTEXT_H
