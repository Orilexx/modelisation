#pragma once
#ifndef EMPRUNT_H
#define EMPRUNT_H

#include "Utilisateur.h"
#include "Outil.h"
#include "Constat.h"
#include <list>
#include <string>
#include <iostream>

using namespace std;

class Utilisateur;
class Outil;
class Constat;
class Emprunt
{
private:
	Utilisateur* emprunteur;
	Utilisateur* preteur;
	list<Outil*> outils;
	string dateEmprunt;
	string dateRetour;
	Constat* constatEmprunt;
	Constat* constatRetour;
	bool demandeApprouve;

public:
	Emprunt();
	Emprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt, Constat* constatRetour, bool demandeApprouve);
	Utilisateur* getPreteur();
	Utilisateur* getEmprunteur();
	list<Outil*> getListOutils();
	string getDateEmprunt();
	string getDateRetour();
	Constat* getConstatEmprunt();
	Constat* getConstatRetour();
	bool getDemandeApprouve();
	void updateEmprunt(Emprunt* emprunt);

};
#endif // !EMPRUNT_H

