#pragma once
#ifndef FORMREMPLI_H
#define FORMREMPLI_H

#include "Emprunt.h"
#include "Strategy.h"

#include <list>
#include <string>

using namespace std;


class FormRempli : public Strategy {

public: Emprunt FillEmprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt, Constat* constatRetour, bool demandeApprouve);


};
#endif // !FORMREMPLI_H