#pragma once
#ifndef OUTIL_H
#define OUTIL_H


#include <string>
#include "Evaluation.h"
#include "Utilisateur.h"
#include "Photo.h"
#include <list>
#include "BuilderOutil.h"

using namespace std;

class BuilderOutil;
class Photo;
class Evaluation;
class Outil
{
private:
	string nom;
	int anneeAchat;
	Photo* photo;
	string dommageRecu;
	list<Evaluation*> evaluations;
	bool empruntable;
	double valeurEstime = 0;

public:

	Outil(string, int, Photo*, string, bool, double);

	Outil(BuilderOutil builder);

	list<Evaluation*> getEvaluations();
	void ajouterEvaluationListe(Evaluation* evaluation);

	string getNom();
	int getAnnee();
	Photo* getPhoto();
	string getDommage();
	bool getEmpruntable();
	double getValeurEstime();
};
#endif 

