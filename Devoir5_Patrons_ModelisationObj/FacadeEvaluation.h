#pragma once
#ifndef FACADEEVALUATION_H
#define FACADEEVALUATION_H


#include "Utilisateur.h"
#include "Outil.h"
#include "Emprunt.h"
#include "Evaluation.h"
#include "Constat.h"
#include <list>
using namespace std;

class Outil;
class Utilisateur;
class Emprunt;
class Evaluation;

class FacadeEvaluation
{
	Utilisateur* utilisateur;
	list<Outil*> outils;
	list<Emprunt*> emprunts;

public:
	//FacadeEvaluation(Utilisateur& utilisateur);
	FacadeEvaluation(Utilisateur* utilisateur);

	list<Evaluation*> ensembleEvaluationsFaitesSurUtilisateur();
};
#endif // !FACADEEVALUATION_H
