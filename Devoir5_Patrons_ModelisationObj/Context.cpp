

#include "Context.h"




 void Context::set_strategy(Strategy* strategy)
 {
     delete this->strategy_;
     this->strategy_ = strategy;
 }

 Emprunt Context::FillEmprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt, Constat* constatRetour, bool demandeApprouve)
 {
     return this->strategy_->FillEmprunt(emprunteur, preteur, outils, dateEmprunt, dateRetour, constatEmprunt, constatRetour, demandeApprouve);
 }
