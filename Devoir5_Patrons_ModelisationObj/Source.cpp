//Devoir 5

#include <string>
#include <iostream>
#include "Outil.h"
#include "BuilderOutil.h"
#include "Evaluation.h"
#include "Photo.h"
#include "Constat.h"
#include "FacadeEvaluation.h"
#include "Utilisateur.h"
#include "Emprunt.h"
#include "Strategy.h"
#include "FormApprouve.h"
#include "FormEmpruntRetourne.h"
#include "FormNonApprouve.h"
#include "FormRempli.h"
#include "Context.h"

#include <cmath>
#include <iostream>

using namespace std;

int main() {

	int choix = 0;
	Adresse adresse = Adresse();
	Photo photo = Photo();
	Utilisateur user_a = Utilisateur("David", "LG", adresse, "eml");
	Utilisateur user_b = Utilisateur("Antoine", "Gagnon", adresse, "yahoo");
	Utilisateur user_c = Utilisateur("Chose", "Bine", adresse, "what");

	cout << "Veuillez ecrire le numero correspondant au patron que vous voulez tester. " << endl << endl;

	cout << "1) Tester le builder. " << endl << "2) Tester la facade. " << endl << "3) Tester la strategy. " << endl << "0) Pour quitter. " << endl << endl; 
	cout << "Votre choix: ";
	cin >> choix;

	while (choix != 0)
	{
		if (choix == 1) {
			cout << "--------------------------------------------------------" << endl;
			cout << "  ---------- Builder: Creer un nouvel outil ----------" << endl;
			cout << "--------------------------------------------------------" << endl << endl;

			Photo* nouvellePhoto = nullptr;	

			Evaluation* evaluation = new Evaluation(&user_b, 2, "Bien", &photo); 
			Evaluation* evaluation2 = new Evaluation(&user_c, 1, "Correct", &photo); 

			BuilderOutil* builder = new BuilderOutil();

			builder->ajouterEvaluationListe(evaluation);
			builder->ajouterEvaluationListe(evaluation2); 

			builder->setNom("Marteau");
			builder->setAnneeAchat(2019);
			builder->setPhoto(nouvellePhoto);
			builder->setDommageRecu("rien");
			builder->setEmpruntable(true);
			builder->setValeurEstime(100);

			Outil* outil = builder->build();
			
			cout << " L'outil est : " << endl;
			cout << " Nom de l'outil : ";
			cout << outil->getNom() << endl;
			cout << " L'annee : ";
			cout << outil->getAnnee() << endl;
			cout << " La photo : ";
			cout << outil->getPhoto() << endl;
			cout << " Dommage : ";
			cout << outil->getDommage() << endl;
			cout << " Empruntable 1(OUI) et 0(NON) : ";
			cout << outil->getEmpruntable() << endl;
			cout << " Valeur estime : ";
			cout << outil->getValeurEstime() << " $" << endl << endl;
			cout << "--------------------------------------------------------" << endl;
			cout << "Le nouvel outil a ete cree avec succes grace au builder! " << endl;
			cout << "--------------------------------------------------------" << endl;

			user_a.ajouterUnOutilPossede(outil);
			system("pause");
			//delete evaluation;
			//delete evaluation2;
			delete builder;

		}
		else if (choix == 2) {
			cout << endl << "Facade: " << endl;

			Evaluation eval1 = Evaluation(&user_b, 4, "bien", &photo);
			user_a.ajouterEvaluationCommeEmprunteur(&eval1);
			Evaluation eval2 = Evaluation(&user_c, 3, "correct", &photo);
			user_a.ajouterEvaluationCommeEmprunteur(&eval2);
			Evaluation eval3 = Evaluation(&user_b, 5, "super", &photo);
			user_a.ajouterEvaluationCommeProprietaire(&eval3);

			FacadeEvaluation facade = FacadeEvaluation(&user_a);
			list<Evaluation*> evaluations = facade.ensembleEvaluationsFaitesSurUtilisateur();
			int sum = 0;
			int nb = 0;
			int note = 0;
			double avg = 0;
			for (Evaluation* evaluation : evaluations)
			{
				note = evaluation->getNote();
				if (note > 0)
				{
					++nb;
					sum += note;
				}
			}
			if (nb > 0)
			{
				avg = sum / nb;
			}
			cout << "Utilisateur : " << user_a.getPrenom() << endl;
			cout << "Note moyenne : " << avg << endl;
			system("pause");
		}
		else if (choix == 3) {
			cout << endl << "Strategie: " << endl;
			Context* context = new Context(new FormRempli);
			Emprunt* emprunt = new Emprunt();
			Constat* constatEmprunt = new Constat(&user_a, "emprunt", new Photo());
			Constat* constatRetour = new Constat(&user_a, "retour", new Photo());
			list<Outil*> outilsEmprunt;

			cout << endl << "Remplissage d'un formulaire d'emprunt par l'emprunteur : " << endl;

			Emprunt empruntContext = context->FillEmprunt(&user_a, &user_b, outilsEmprunt, "2021-06-02", "2021-06-25", constatEmprunt, constatRetour, false);
			emprunt->updateEmprunt(&empruntContext);
			cout << "Emprunteur : " << emprunt->getEmprunteur()->getPrenom() << endl;
			cout << "Preteur : " << emprunt->getPreteur()->getPrenom() << endl;
			cout << "Date d'emprunt : " << emprunt->getDateEmprunt() << endl;
			cout << "Date de retour : " << emprunt->getDateRetour() << endl;
			cout << "Constat d'emprunt " << emprunt->getConstatEmprunt() << endl;
			cout << "Constat de retour " << emprunt->getConstatRetour() << endl;
			cout << "Demmande approuvee : " << emprunt->getDemandeApprouve() << endl;

			cout << endl << "Remplissage d'un formulaire d'emprunt approuve par le preteur : " << endl;
			
			context->set_strategy(new FormApprouve);
			
			empruntContext = context->FillEmprunt(&user_a, &user_b, outilsEmprunt, "2021-06-02", "2021-06-25", constatEmprunt, constatRetour, true);
			emprunt->updateEmprunt(&empruntContext);
			cout << "Emprunteur : " << emprunt->getEmprunteur()->getPrenom() << endl;
			cout << "Preteur : " << emprunt->getPreteur()->getPrenom() << endl;
			cout << "Date d'emprunt : " << emprunt->getDateEmprunt() << endl;
			cout << "Date de retour : " << emprunt->getDateRetour() << endl;
			cout << "Constat d'emprunt " << emprunt->getConstatEmprunt() << endl;
			cout << "Constat de retour " << emprunt->getConstatRetour() << endl;
			cout << "Demmande approuvee : " << emprunt->getDemandeApprouve() << endl;
			
			cout << endl << "Remplissage d'un formulaire d'emprunt non approuve par le preteur : " << endl;

			context->set_strategy(new FormNonApprouve);

			empruntContext = context->FillEmprunt(&user_a, &user_b, outilsEmprunt, "2021-06-02", "2021-06-25", constatEmprunt, constatRetour, true);
			emprunt->updateEmprunt(&empruntContext);
			cout << "Emprunteur : " << emprunt->getEmprunteur()->getPrenom() << endl;
			cout << "Preteur : " << emprunt->getPreteur()->getPrenom() << endl;
			cout << "Date d'emprunt : " << emprunt->getDateEmprunt() << endl;
			cout << "Date de retour : " << emprunt->getDateRetour() << endl;
			cout << "Constat d'emprunt " << emprunt->getConstatEmprunt() << endl;
			cout << "Constat de retour " << emprunt->getConstatRetour() << endl;
			cout << "Demmande approuvee : " << emprunt->getDemandeApprouve() << endl;

			cout << endl << "Remplissage d'un formulaire d'emprunt lorsque les outils ont ete rendus : " << endl;

			context->set_strategy(new FormEmpruntRetourne);
			
			empruntContext = context->FillEmprunt(&user_a, &user_b, outilsEmprunt, "2021-06-02", "2021-06-25", constatEmprunt, constatRetour, true);
			emprunt->updateEmprunt(&empruntContext);
			cout << "Emprunteur : " << emprunt->getEmprunteur()->getPrenom() << endl;
			cout << "Preteur : " << emprunt->getPreteur()->getPrenom() << endl;
			cout << "Date d'emprunt : " << emprunt->getDateEmprunt() << endl;
			cout << "Date de retour : " << emprunt->getDateRetour() << endl;
			cout << "Constat d'emprunt " << emprunt->getConstatEmprunt() << endl;
			cout << "Constat de retour " << emprunt->getConstatRetour() << endl;
			cout << "Demmande approuvee : " << emprunt->getDemandeApprouve() << endl;

			system("pause");
		}
		else {
			cout << "Input invalide " << endl;
			system("pause");

		}
		system("cls");
		cout << "1) Tester le builder. " << endl << "2) Tester la facade. " << endl << "3) Tester la strategy. " << endl << "0) Pour quitter. " << endl << endl;
		cout << "Votre choix: ";
		cin >> choix;
	}

	system("pause");
	return 0;
}
