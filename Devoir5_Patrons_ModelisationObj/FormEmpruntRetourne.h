#pragma once
#ifndef FORMEMPRUNTRETOURNE_H
#define FORMEMPRUNTRETOURNE_H

#include "Emprunt.h"
#include "Strategy.h"

#include <list>
#include <string>

using namespace std;


class FormEmpruntRetourne : public Strategy {

public: Emprunt FillEmprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt, Constat* constatRetour, bool demandeApprouve);
	    

};
#endif // !FORMEMPRUNTRETOURNE_H