#include "Evaluation.h"
#include <iostream>

using namespace std;

Evaluation::Evaluation(Utilisateur* evaluateur, int note, string commentaire, Photo* photo)
{
	this->evaluateur = evaluateur;
	this->note = note;
	this-> commentaire = commentaire;
	this->photoOutil = photo;

}

Utilisateur* Evaluation::getEvaluateur()
{
	return this->evaluateur;
}

void Evaluation::imprimerEvaluation()
{
	//Utilisateur* usr =  Utilisateur(this->evaluateur);
	//usr=getEvaluateur();
	
	cout << "--------------------------->" << endl;
	cout << "Evaluateur: " <<  endl;
	cout << "Note: " << this->note << endl;
	cout << "Commentaire: " << this->commentaire << endl;
	cout << "<---------------------------";
}

int Evaluation::getNote()
{
	return this->note;
}
