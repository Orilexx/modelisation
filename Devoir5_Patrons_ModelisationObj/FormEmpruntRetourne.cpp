#include "Emprunt.h"
#include "Strategy.h"
#include "FormEmpruntRetourne.h"

#include <list>
#include <string>


Emprunt FormEmpruntRetourne::FillEmprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt, Constat* constatRetour, bool demandeApprouve = true)
{

	return Emprunt(emprunteur, preteur, outils, dateEmprunt, dateRetour, constatEmprunt, constatRetour, true);

}
