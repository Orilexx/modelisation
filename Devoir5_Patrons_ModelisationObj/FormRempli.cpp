#include "Emprunt.h"
#include "Strategy.h"
#include "FormRempli.h"

#include <list>
#include <string>

Emprunt FormRempli::FillEmprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt = nullptr, Constat* constatRetour = nullptr, bool demandeApprouve = false)
{
	return Emprunt(emprunteur, preteur, outils, dateEmprunt, dateRetour, nullptr, nullptr, false);
}
