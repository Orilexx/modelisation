#pragma once
#ifndef FORMNONAPPROUVE_H
#define FORMNONAPPROUVE_H

#include "Emprunt.h"
#include "Strategy.h"

#include <list>
#include <string>

using namespace std;


class FormNonApprouve : public Strategy {

public: Emprunt FillEmprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt, Constat* constatRetour, bool demandeApprouve);


};
#endif // !FORMNONAPPROUVE_H