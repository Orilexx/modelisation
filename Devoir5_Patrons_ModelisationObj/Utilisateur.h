#pragma once
#ifndef UTILISATEUR_H
#define UTILISATEUR_H

#include "Adresse.h"
#include "Outil.h"
#include "Evaluation.h"
#include "Emprunt.h"
#include <list>
#include <string>
#include <iostream>

using namespace std;

class Outil;
class Evaluation;
class Emprunt;
class Utilisateur
{
private:
	string prenom;
	string nom;
	Adresse adresse;
	long carteCredit = 0;
	string courriel;
	list<Outil*> outils;
	list<Evaluation*> evaluationsEmprunteur;
	list<Evaluation*> evaluationsProprietaire;
	list<Emprunt*> empruntsPreteur;
	list<Emprunt*> empruntsEmprunteur;
	bool estMembre = false;

public:
	Utilisateur();
	Utilisateur(string prenom, string nom, Adresse adresse, string courriel);
	void devenirMembre(long carteCredit);
	void ajouterUnOutilPossede(Outil* outil);
	void ajouterEvaluationCommeEmprunteur(Evaluation* evaluation);
	void ajouterEvaluationCommeProprietaire(Evaluation* evaluation);
	list<Outil*> getOutils();
	list<Evaluation*> getEvaluationsEmprunteur();
	list<Evaluation*> getEvaluationProprietaire();
	list<Emprunt*> getEmpruntsPreteur();
	list<Emprunt*> getEmpruntsEmprunteur();
	string getPrenom();
};
#endif 

