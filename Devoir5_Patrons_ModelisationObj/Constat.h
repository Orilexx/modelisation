#pragma once
#ifndef CONSTAT_H
#define CONSTAT_H

#include <string>
#include "Photo.h"
#include "Utilisateur.h"
#include "Evaluation.h"
#include <iostream>
using namespace std;

class Utilisateur;
class Photo;
class Constat: public Evaluation{

private:

	bool approuveEmprunteur = false;
	bool approuveProprietaire= false;

public:

	Constat(Utilisateur*, string, Photo*);

	void setApprouveEmprunteur(bool approuveEmprunteur);
	void setApprouveProprietaire(bool approuveProprietaire);
};
#endif 

