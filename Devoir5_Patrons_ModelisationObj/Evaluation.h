#pragma once
#ifndef EVALUATION_H_
#define EVALUATION_H_

//#include "Utilisateur.h"		**COMMANDE MAUDITE DECOMMENTEZ PAS CELA OU TOUTE CRASH !!! - J-F G :) **
#include <string>
#include "Photo.h"

using namespace std;

class Photo;
class Utilisateur;
class Evaluation
{
private:
	Utilisateur* evaluateur;
	int note =0;
	string commentaire;
	Photo* photoOutil;

public:
	Evaluation(Utilisateur* evaluateur, int note, string commentaire, Photo* photo);
	Utilisateur* getEvaluateur();
	void imprimerEvaluation();
	int getNote();
};
#endif 

