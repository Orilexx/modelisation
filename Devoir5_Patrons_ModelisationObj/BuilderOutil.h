#pragma once
#ifndef BUILDEROUTIL_H
#define BUILDEROUTIL_H

#include "Outil.h"
#include <string>
#include "Evaluation.h"
#include "Photo.h"
using namespace std;


class Outil;
class Photo;
class Evaluation;
class BuilderOutil
{
private:
	string _nom;
	int _anneeAchat;
	Photo* _photo;
	string _dommageRecu;
	list<Evaluation*> Liste_evaluations;
	bool _empruntable;
	double _valeurEstime = 0;

public:
	BuilderOutil() {};

	Outil* build();
	void ajouterEvaluationListe(Evaluation* evaluation);

	string getNom();
	int getAnnee();
	Photo* getPhoto();
	string getDommage();
	list<Evaluation*> getEvaluation();
	bool getEmpruntable();
	double getValeurEstime();


	void setNom(string);
	void setAnneeAchat(int);
	void setPhoto(Photo*);
	void setDommageRecu(string);
	//void setEvaluation(Evaluation*);
	void setEmpruntable(bool);
	void setValeurEstime(double);
};
#endif