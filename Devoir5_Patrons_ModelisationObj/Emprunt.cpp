#include "Emprunt.h"
#include <list>

Emprunt::Emprunt() {
    list<Outil*> outils;
    this->emprunteur = nullptr;
    this->preteur = nullptr;
    this->outils = outils;
    this->dateEmprunt = "";
    this->dateRetour = "";
    this->constatEmprunt = nullptr;
    this->constatRetour = nullptr;
    this->demandeApprouve = false;
}

Emprunt::Emprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt, Constat* constatRetour, bool demandeApprouve) {
    this->emprunteur = emprunteur;
    this->preteur = preteur;
    this->outils = outils;
    this->dateEmprunt = dateEmprunt;
    this->dateRetour = dateRetour;
    this->constatEmprunt = constatEmprunt;
    this->constatRetour = constatRetour;
    this->demandeApprouve = demandeApprouve;
}

Utilisateur* Emprunt::getEmprunteur()
{
    return this->emprunteur;
}

Utilisateur* Emprunt::getPreteur()
{
    return this->preteur;
}

list<Outil*> Emprunt::getListOutils()
{
    return this->outils;
}

string Emprunt::getDateEmprunt()
{
    return this->dateEmprunt;
}

string Emprunt::getDateRetour()
{
    return this->dateRetour;
}

bool Emprunt::getDemandeApprouve()
{
    return this->demandeApprouve;
}

Constat* Emprunt::getConstatEmprunt()
{
    return this->constatEmprunt;
}

Constat* Emprunt::getConstatRetour()
{
    return this->constatRetour;
}

void Emprunt::updateEmprunt(Emprunt* emprunt)
{
    this->emprunteur = emprunt->emprunteur;
    this->preteur = emprunt->preteur;
    this->outils = emprunt->outils;
    this->dateEmprunt = emprunt->dateEmprunt;
    this->dateRetour = emprunt->dateRetour;
    this->constatEmprunt = emprunt->constatEmprunt;
    this->constatRetour = emprunt->constatRetour;
    this->demandeApprouve = emprunt->demandeApprouve;
}