#include "Emprunt.h"
#include "Strategy.h"
#include "FormApprouve.h"

#include <list>
#include <string>


Emprunt FormApprouve::FillEmprunt(Utilisateur* emprunteur, Utilisateur* preteur, list<Outil*> outils, string dateEmprunt, string dateRetour, Constat* constatEmprunt, Constat* constatRetour = nullptr, bool demandeApprouve = true)
{
	return Emprunt(emprunteur, preteur, outils, dateEmprunt, dateRetour, constatEmprunt, nullptr, true);
}
