#include "Utilisateur.h"

using namespace std;

Utilisateur::Utilisateur(){}

Utilisateur::Utilisateur(string prenom, string nom, Adresse adresse, string courriel)
{
	this->prenom = prenom;
	this->nom = nom;
	this->adresse = adresse;
	this->courriel = courriel;
}

void Utilisateur::ajouterUnOutilPossede(Outil* outil)
{
	this->outils.push_back(outil);
}

void Utilisateur::ajouterEvaluationCommeEmprunteur(Evaluation* evaluation)
{
	this->evaluationsEmprunteur.push_back(evaluation);
}

void Utilisateur::ajouterEvaluationCommeProprietaire(Evaluation* evaluation)
{
	this->evaluationsProprietaire.push_back(evaluation);
}

list<Outil*> Utilisateur::getOutils()
{
	return this->outils;
}

list<Evaluation*> Utilisateur::getEvaluationsEmprunteur()
{
	return this->evaluationsEmprunteur;
}

list<Evaluation*> Utilisateur::getEvaluationProprietaire()
{
	return this->evaluationsProprietaire;
}

list<Emprunt*> Utilisateur::getEmpruntsPreteur()
{
	return list<Emprunt*>();
}

list<Emprunt*> Utilisateur::getEmpruntsEmprunteur()
{
	return list<Emprunt*>();
}

string Utilisateur::getPrenom()
{
	return this->prenom;
}
